﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnTriggerEnter(Collider col) {
        if (col.tag == "Note")
        {
            GameObject.Find("Main Camera").GetComponent<test>().streak = 0;
            Destroy(col.gameObject);
        }
        else
        {
            print("End?");
            GameObject.Find("Cube").GetComponent<CameraController>().speed = 0;

        }
    }

}
