﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ChartLoader.NET.Utils;
using ChartLoader.NET.Framework;

public class ChartLoaderExample2 : MonoBehaviour {
	public static ChartReader chartReader;
	public float speed = 1f;
	public Transform[] notePrefabs;

	// Use this for initialization
	void Start () {
		chartReader = new ChartReader ();
		//print (":)");
		Chart TroublesChart = chartReader.ReadChartFile ("C:\\Users\\Nbrooks3\\Documents\\New Unity Project (2)\\Assets\\ChartLoader\\ChartLoader\\Songs\\EverybodyTalks\\EverybodyTalks.chart");
        //print ("LOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOL");
        Note[] expertGuitarNotes = TroublesChart.GetNotes ("ExpertSingle");
		SpawnNotes (expertGuitarNotes);
	}

	// Spawn all notes
	public void SpawnNotes(Note[] notes) {
		foreach (Note note in notes) {
			SpawnNote (note);
		}
        SpawnEnd(notes[notes.Length - 1]);
        //SpawnEnd(notes[50]);
	}

	// Spawn single note
	public void SpawnNote(Note note) {
		Vector3 point;
		for (int i = 0; i < note.ButtonIndexes.Length; i++) {
			if (note.ButtonIndexes [i]) {
				point = new Vector3(i-2f, 0f, note.Seconds * speed  - 0.1f);
				SpawnPrefab(notePrefabs[i], point);
			}
		}
	}

	// Spawns a prefab
	public void SpawnPrefab(Transform prefab, Vector3 point) {
		Transform tmp = Instantiate(prefab);
		tmp.SetParent(transform);
		tmp.position = point;
	}

    public void SpawnEnd(Note note)
    {
        Vector3 point;
        point = new Vector3(0f, 0f, (note.Seconds + 5) * speed - 0.1f);
        GameObject.Find("End").transform.position = point;
    }

}
