﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardInput : MonoBehaviour {

	public bool Apress = false;
	public bool Spress = false;
	public bool Dpress = false;
	public bool Fpress = false;
	public bool Gpress = false;
	public bool Strum = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.A)) {
			Apress = true;
		}
		if (Input.GetKey (KeyCode.S)) {
			Spress = true;
		}
		if (Input.GetKey (KeyCode.D)) {
			Dpress = true;
		}
		if (Input.GetKey (KeyCode.F)) {
			Fpress = true;
		}
		if (Input.GetKey (KeyCode.G)) {
			Gpress = true;
		}
		if (Input.GetKeyDown (KeyCode.RightShift)) {
			Strum = true;
		}
		if (Apress && Strum) {
			Debug.Log("A has been pressed!");
		}
		if (Spress && Strum) {
			Debug.Log("S has been pressed!");
		}
		if (Dpress && Strum) {
			Debug.Log("D has been pressed!");
		}
		if (Fpress && Strum) {
			Debug.Log("F has been pressed!");
		}
		if (Gpress && Strum) {
			Debug.Log("G has been pressed!");
		}
		if (Strum && !Apress && !Spress && !Dpress && !Fpress && !Gpress) {
			Debug.Log("Strum was hit, no color");
		}
		Apress = false;
		Spress = false;
		Dpress = false;
		Fpress = false;
		Gpress = false;
		Strum = false;
	}
}
