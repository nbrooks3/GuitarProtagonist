﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text;
using System;
using WiimoteApi;

public class NoteDetector : MonoBehaviour {
	public KeyCode key;
    private Wiimote wiimote;
	//bool active = false;
	//GameObject note;
    public double strumwindow = 0.12;

	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {
	}

    void OnTriggerEnter(Collider col)
    {
        //active = true;
        /*if (Input.GetKey (key)) {
			Destroy (col.gameObject);
		}*/

        bool green;
        bool yellow;
        bool red;
        bool blue;
        bool orange;

        bool strum;
        float strum_start;
        float strum_delta;
        green = GameObject.Find("Main Camera").GetComponent<test>().Apress;
        red = GameObject.Find("Main Camera").GetComponent<test>().Spress;
        yellow = GameObject.Find("Main Camera").GetComponent<test>().Dpress;
        blue = GameObject.Find("Main Camera").GetComponent<test>().Fpress;
        orange = GameObject.Find("Main Camera").GetComponent<test>().Gpress;
        strum = GameObject.Find("Main Camera").GetComponent<test>().Strum;
        strum_start = GameObject.Find("Main Camera").GetComponent<test>().StrumStart;

        strum_delta = Time.time - strum_start;
        /*if (green)
        {
            print("Status");
            print(strum_start);
            print(strum_delta);
        }*/
        

        int x;
        x = (int)col.gameObject.transform.position.x;
        if (x == -2 && green && strum && strum_delta < strumwindow)
        {
            print("hit green");
            GameObject.Find("Main Camera").GetComponent<test>().score += 1;
            GameObject.Find("Main Camera").GetComponent<test>().streak += 1;

            Destroy(col.gameObject);
        }
        if (x == -1 && red && strum && strum_delta < strumwindow)
        {
            print("hit red");
            GameObject.Find("Main Camera").GetComponent<test>().score += 1;
            GameObject.Find("Main Camera").GetComponent<test>().streak += 1;

            Destroy(col.gameObject);
        }
        if (x == 0 && yellow && strum && strum_delta < strumwindow)
        {
            print("hit yellow");
            GameObject.Find("Main Camera").GetComponent<test>().score += 1;
            GameObject.Find("Main Camera").GetComponent<test>().streak += 1;

            Destroy(col.gameObject);
        }
        if (x == 1 && blue && strum && strum_delta < strumwindow)
        {
            print("hit blue");
            GameObject.Find("Main Camera").GetComponent<test>().score += 1;
            GameObject.Find("Main Camera").GetComponent<test>().streak += 1;

            Destroy(col.gameObject);
        }
        if (x == 2 && orange && strum && strum_delta < strumwindow)
        {
            print("hit orange");
            GameObject.Find("Main Camera").GetComponent<test>().score += 1;
            GameObject.Find("Main Camera").GetComponent<test>().streak += 1;

            Destroy(col.gameObject);
        }
    }

    void OnTriggerExit(Collider col) {
     // active = false;
    }
}
