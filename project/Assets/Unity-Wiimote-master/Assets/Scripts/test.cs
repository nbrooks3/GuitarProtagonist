﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text;
using System;
using WiimoteApi;

public class test : MonoBehaviour
{
	public bool Apress = false;
	public bool Spress = false;
	public bool Dpress = false;
	public bool Fpress = false;
	public bool Gpress = false;
	public bool Strum = false;
    public float StrumStart = 0;

    public int score = 0;
    public int streak = 0;
    private Wiimote wiimote;


    void Start()
    {
        WiimoteManager.FindWiimotes();
        if (!WiimoteManager.HasWiimote()) { return; }

        wiimote = WiimoteManager.Wiimotes[0];
        wiimote.SendDataReportMode(InputDataType.REPORT_BUTTONS_ACCEL_EXT16);
        wiimote.SendPlayerLED(true, false, false, false);

        GameObject.Find("Score").GetComponent<Text>().text = "Score: " + score;
        GameObject.Find("Streak").GetComponent<Text>().text = "Streak: " + streak;
    }

    void Update()
    {
        WiimoteManager.FindWiimotes();
        if (!WiimoteManager.HasWiimote()) { return; }

        wiimote = WiimoteManager.Wiimotes[0];
        int ret;
        ret = wiimote.ReadWiimoteData();
        if (wiimote != null && wiimote.current_ext != ExtensionController.NONE)
        {
            if (wiimote.current_ext == ExtensionController.GUITAR)
            {
                //Apress = false;
                //Spress = false;
                //Dpress = false;
                //Fpress = false;
                //Gpress = false;
                //Strum = false;
                GuitarData data = wiimote.Guitar;
                float[] stick = data.GetStick01();
                /*if (data.green)
                {
                    print("Stick: " + stick[0] + ", " + stick[1]);
                    print("Slider: " + (data.has_slider ? Convert.ToString(data.GetSlider01()) : "unsupported"));
                    print("Green: " + data.green);
                    print("Red: " + data.red);
                    print("Yellow: " + data.yellow);
                    print("Blue: " + data.blue);
                    print("Orange: " + data.orange);
                    print("Strum Up: " + data.strum_up);
                    print("Strum Down: " + data.strum_down);
                    print("Minus: " + data.minus);
                    print("Plus: " + data.plus);
                    print("Whammy: " + data.GetWhammy01());
                }*/
				if (data.green) {
					Apress = true;
				} else
                {
                    Apress = false;
                }
				if (data.red) {
					Spress = true;
				}
                else
                {
                    Spress = false;
                }
                if (data.yellow) {
					Dpress = true;
				}
                else
                {
                    Dpress = false;
                }
                if (data.blue) {
					Fpress = true;
				}
                else
                {
                    Fpress = false;
                }
                if (data.orange) {
					Gpress = true;
				}
                else
                {
                    Gpress = false;
                }
                if (data.strum_up || data.strum_down) {
                    if (Strum == false)
                    {
                        StrumStart = Time.time;
                    }
					Strum = true;
				} else
                {
                    Strum = false;
                }
                /*if (Apress && Strum) {
					Debug.Log("A has been pressed!");
				}
				if (Spress && Strum) {
					Debug.Log("S has been pressed!");
				}
				if (Dpress && Strum) {
					Debug.Log("D has been pressed!");
				}
				if (Fpress && Strum) {
					Debug.Log("F has been pressed!");
				}
				if (Gpress && Strum) {
					Debug.Log("G has been pressed!");
				}
				if (Strum && !Apress && !Spress && !Dpress && !Fpress && !Gpress) {
					Debug.Log("Strum was hit, no color");
				}*/
                GameObject.Find("Score").GetComponent<Text>().text = "Score: " + score;
                GameObject.Find("Streak").GetComponent<Text>().text = "Streak: " + streak;

            }
        }

    }


    void OnApplicationQuit()
    {
        if (wiimote != null)
        {
            WiimoteManager.Cleanup(wiimote);
            wiimote = null;
        }
    }
}
